import UIKit

class TrainingViewController: UIViewController {

	var challenges: [Challenge]! = nil
	var challengeIndex: Int = 0
	var treatsEarned = -1

	// MARK: - IB Outlets

	@IBOutlet weak var challengeLabel: UILabel!
	@IBOutlet weak var challengeHint: UILabel!
	@IBOutlet weak var questionLabel: UILabel!
	@IBOutlet weak var treatCountLabel: UILabel!
	@IBOutlet weak var treatTimeStackView: UIStackView!
	@IBOutlet weak var resultStackView: UIStackView!

	// MARK: - IB Actions

    @IBAction func didTapCrashButton(_ sender: UIButton) {
        let numbers = [0]
        let _ = numbers[1]
    }

	@IBAction func nextButtonTapped(_ sender: UIButton) {
		presentQuestion()
	}

	@IBAction func yesButtonTapped(_ sender: UIButton) {
		handleResponse()
	}
	@IBAction func humanYesButtonTapped(_ sender: UIButton) {
		handleResponse()
	}
	@IBAction func noButtonTapped(_ sender: UIButton) {
		handleResponse()
	}

	// MARK: - View Lifecycle

	override func viewDidLoad() {
		let model = TrainingModel()
		challenges = model.challenges
		challengeIndex = challenges.count - 1

		presentQuestion()
	}

	// MARK: - Class Methods

  ///Present next Challenge and Update data and UI
	func presentQuestion() {
		challengeIndex = (challengeIndex + 1) % challenges.count
		challengeLabel.text = challenges[challengeIndex].action
		challengeHint.text = challenges[challengeIndex].hint

		questionLabel.text = "Did your dog \(challenges[challengeIndex].action.lowercased())?"

		treatsEarned += 1
		treatCountLabel.text = treatsEarned == 0 ? "" : "\(treatsEarned) Tasty Treats Enjoyed!"

		// TODO : animate
		treatTimeStackView.isHidden = true
		resultStackView.isHidden = false
	}

  ///Respond to UI challenge button taps
	func handleResponse() {

		// TODO : animate
		treatTimeStackView.isHidden = false
		resultStackView.isHidden = true

	}
}
