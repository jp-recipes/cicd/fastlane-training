import Foundation

struct Challenge {
	let action: String
	let hint: String
}

struct TrainingModel {
	let challenges = [
    Challenge(action: "Sit", hint: "Take a seat yourself. The best teachers teach by example."),
    Challenge(action: "Stay", hint: "Try running around in circles and repeating \"Do the opposite.\""),
    Challenge(action: "Heel", hint: "Try removing your shoe and pointing to the heel. Dogs are visually oriented."),
    Challenge(action: "Bark", hint: "If you're in a public area, bark as loudly as you can. You'll make new acquaintances"),
    Challenge(action: "Yodel", hint: "If you don't yodel professionally, consider learning. Your dog and friends will thank you."),
    Challenge(action: "Be Quiet", hint: "Donning classic Mime garb can yield surprising results."),
    Challenge(action: "Jump", hint: "Leading by example works great but is tiring. Instead, try sending your dog telepathic \"jump\" thoughts .")	]
}
