//
//  Fastlane_Snapshots.swift
//  Fastlane Snapshots
//
//  Created by jp on 01-04-23.
//

import XCTest

final class Fastlane_Snapshots: XCTestCase {

    override func setUp() {
        let app = XCUIApplication()
        setupSnapshot(app)
        app.launch()
    }

    func testSnapshot() {
        let app = XCUIApplication()
        snapshot("1Launch")
        app.staticTexts["How's it Work?"].tap()
        snapshot("2HowItWorks")
        app.staticTexts["Sounds Fun!"].tap()
        snapshot("3BecauseScience")
        app.staticTexts["Let's Go!"].tap()
        snapshot("4Challenge")
        app.staticTexts["Try again"].tap()
        snapshot("5Reward")
    }

}
